import sys
sys.path.append('./scripts/')
import readProfile
import plot_profile
import process_profile

import matplotlib.pyplot as plt
import datetime

#%%
filename = './input/WFJ2.pro'
prof = readProfile.read_profile(filename,remove_soil=True)
prof = process_profile.process_profile(prof)

#%%

fig=plt.figure(figsize=(4,8))
ax=fig.add_axes([0.17,0.08,0.63,0.85])
axcolor=fig.add_axes([0.85,0.08,0.05,0.85])

ts=datetime.datetime(2022,2,1,12)
pro = prof['data'][ts]
ax11 = plot_profile.plot_single_profile(fig, ax, pro, var='density',ax_colorbar=axcolor)

figname = './plots/WFJ2_220201.png'
plt.savefig(figname)

#%%

fig, ax = plt.subplots(1,1, figsize = (12,6))

cb=plot_profile.plot_seasonal_evolution(ax, prof, var='lwc', vmax=4)

figname = './plots/WFJ2_evo_lwc.png'
plt.savefig(figname)
#%%

fig, ax = plt.subplots(1,1, figsize = (12,6))

cb=plot_profile.plot_seasonal_evolution(ax, prof, var='density', vmax=550)

figname = './plots/WFJ2_evo_density.png'
plt.savefig(figname)

#%%

fig, ax = plt.subplots(1,1, figsize = (12,6))

cb=plot_profile.plot_seasonal_evolution(ax, prof)

figname = './plots/WFJ2_evo_graintype.png'
plt.savefig(figname)




